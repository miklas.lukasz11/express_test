const express = require('express');
const app = express();
const port = 3000;
const hostname = '0.0.0.0';
var path = require('path');

app.get('/', (req, res) => {
  var str = '';
  var url = require('url');
  var requrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
  });
  app._router.stack.forEach(function (r) {
    if (r.route && r.route.path) {
      console.log(r.route.path);

      str +=
        '<a href="' + requrl + r.route.path + '">' + r.route.path + '</a></br>';
    }
  });
  res.send(str);
});

app.get('/get', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/err', (req, res) => {
  throw 'You shall not pass';
});

app.get('/err2', (req, res) => {
  throw 'Luke, I am your father';
});
app.get('/InternalError', (req, res) => {
  throw InternalError();
  res.send(req.originalUrl);
});

app.get('/RangeError', (req, res) => {
  throw RangeError();
  res.send(req.originalUrl);
});

app.get('/ReferenceError', (req, res) => {
  throw ReferenceError();
  res.send(req.originalUrl);
});

app.get('/SyntaxError', (req, res) => {
  throw SyntaxError();
  res.send(req.originalUrl);
});

app.get('/TypeError', (req, res) => {
  throw TypeError();
  res.send(req.originalUrl);
});

app.get('/URIError', (req, res) => {
  throw URIError();
  res.send(req.originalUrl);
});

app.get('/test', (req, res) => {
  var name = req.param('name');
  console.log(name);
  res.send(req.originalUrl);
});

async function test(host_, port_, path_) {
  const http = require('http');
  const options = {
    hostname: host_,
    port: port_,
    path: path_,
    method: 'GET',
  };
  const req = http.request(options, (res) => {
    //console.log(`statusCode: ${res.statusCode}`);

    res.on('data', (d) => {
      process.stdout.write(d);
    });
  });

  req.on('error', (error) => {
    console.error(error);
  });

  return req.end();
}

app.listen(port, hostname, () => {
  console.log(`Example app listening at http://${hostname}:${port}`);
});
